import { Degree, DirectionCommand, DirectionCommandZ } from './types';

/**
 * Represents a toy robot that can move on a square 5x5 tabletop.
 */
class ToyRobot {
  private x = 0;
  private y = 0;
  private degree: number | null = null;

  /**
   * Place the toy robot on the table at a specific position and direction.
   * @param {number} x - The X-coordinate.
   * @param {number} y - The Y-coordinate.
   * @param {string} facing - The direction (NORTH, SOUTH, EAST, WEST).
   */
  public place(x: number, y: number, facing: string): void {
    if (this.isValidPosition(x, y) && this.isValidDirection(facing)) {
      const direction = DirectionCommandZ.parse(facing);
      this.x = x;
      this.y = y;
      this.degree = this.getDirectionValue(direction);
    }
  }

  /**
   * Move the toy robot one unit forward in the current direction.
   */
  public move(): void {
    if (
      this.degree === Degree.NORTH &&
      this.isValidPosition(this.x, this.y + 1)
    ) {
      this.y += 1;
    } else if (
      this.degree === Degree.SOUTH &&
      this.isValidPosition(this.x, this.y - 1)
    ) {
      this.y -= 1;
    } else if (
      this.degree === Degree.EAST &&
      this.isValidPosition(this.x + 1, this.y)
    ) {
      this.x += 1;
    } else if (
      this.degree === Degree.WEST &&
      this.isValidPosition(this.x - 1, this.y)
    ) {
      this.x -= 1;
    }
  }

  /**
   * Rotate the toy robot 90 degrees to the left.
   */
  public left(): void {
    this.rotate(-90);
  }

  /**
   * Rotate the toy robot 90 degrees to the right.
   */
  public right(): void {
    this.rotate(90);
  }

  /**
   * Rotate the toy robot by a specified number of degrees.
   * @param {number} degrees - The number of degrees to rotate (positive or negative).
   */
  private rotate(degrees: number): void {
    if (this.degree !== null) {
      this.degree = (this.degree + degrees) % 360;

      if (this.degree < 0) {
        this.degree += 360;
      }
    }
  }

  /**
   * Report the current position and direction of the toy robot.
   * @returns {string} - A string describing the position and direction.
   */
  public report(): string {
    if (this.degree !== null) {
      return `${this.x},${this.y},${this.getDegreeToDirection()}`;
    }
    return 'Robot not placed on the table';
  }

  /**
   * Check if a position is valid within the tabletop boundaries.
   * @param {number} x - The X-coordinate.
   * @param {number} y - The Y-coordinate.
   * @returns {boolean} - True if the position is valid, false otherwise.
   */
  private isValidPosition(x: number, y: number): boolean {
    return x >= 0 && x <= 4 && y >= 0 && y <= 4;
  }

  /**
   * Check if a direction is valid.
   * @param {string | null} direction - The direction (NORTH, SOUTH, EAST, WEST).
   * @returns {boolean} - True if the direction is valid, false otherwise.
   */
  private isValidDirection(direction: string | null): boolean {
    return DirectionCommandZ.safeParse(direction).success;
  }

  /**
   * Convert a DirectionCommand value to degrees.
   * @param {DirectionCommand} direction - The direction value.
   * @returns {number} - The corresponding degree value.
   */
  private getDirectionValue(direction: DirectionCommand): number {
    switch (direction) {
      case DirectionCommand.NORTH:
        return Degree.NORTH;
      case DirectionCommand.SOUTH:
        return Degree.SOUTH;
      case DirectionCommand.EAST:
        return Degree.EAST;
      case DirectionCommand.WEST:
        return Degree.WEST;
      default:
        return 0;
    }
  }

  /**
   * Convert the current degree value to a DirectionCommand value.
   * @returns {DirectionCommand | null} - The corresponding DirectionCommand value.
   */
  private getDegreeToDirection(): DirectionCommand | null {
    switch (this.degree) {
      case Degree.NORTH:
        return DirectionCommand.NORTH;
      case Degree.SOUTH:
        return DirectionCommand.SOUTH;
      case Degree.EAST:
        return DirectionCommand.EAST;
      case Degree.WEST:
        return DirectionCommand.WEST;
      default:
        return null;
    }
  }
}

export default ToyRobot;
