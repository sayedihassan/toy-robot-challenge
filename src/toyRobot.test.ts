import ToyRobot from './toyRobot';

describe('ToyRobot', () => {
  let robot: ToyRobot;

  beforeEach(() => {
    robot = new ToyRobot();
  });

  describe('place', () => {
    it('should place the robot at the specified position and direction', () => {
      robot.place(2, 3, 'NORTH');
      expect(robot.report()).toBe('2,3,NORTH');
    });

    it('should not place the robot if the position is invalid', () => {
      robot.place(5, 5, 'NORTH');
      expect(robot.report()).toBe('Robot not placed on the table');
    });

    it('should not place the robot if the direction is invalid', () => {
      robot.place(2, 3, 'INVALID_DIRECTION');
      expect(robot.report()).toBe('Robot not placed on the table');
    });
  });

  describe('move', () => {
    it('should move the robot one unit NORTH', () => {
      robot.place(2, 2, 'NORTH');
      robot.move();
      expect(robot.report()).toBe('2,3,NORTH');
    });

    it('should move the robot one unit SOUTH', () => {
      robot.place(2, 2, 'SOUTH');
      robot.move();
      expect(robot.report()).toBe('2,1,SOUTH');
    });

    it('should move the robot one unit EAST', () => {
      robot.place(2, 2, 'EAST');
      robot.move();
      expect(robot.report()).toBe('3,2,EAST');
    });

    it('should move the robot one unit WEST', () => {
      robot.place(2, 2, 'WEST');
      robot.move();
      expect(robot.report()).toBe('1,2,WEST');
    });

    it('should not move if the robot is not placed on the table', () => {
      robot.move();
      expect(robot.report()).toBe('Robot not placed on the table');
    });

    it('should not move NORTH if it would fall off the table', () => {
      robot.place(0, 4, 'NORTH');
      robot.move();
      expect(robot.report()).toBe('0,4,NORTH');
    });

    it('should not move SOUTH if it would fall off the table', () => {
      robot.place(0, 0, 'SOUTH');
      robot.move();
      expect(robot.report()).toBe('0,0,SOUTH');
    });

    it('should not move EAST if it would fall off the table', () => {
      robot.place(4, 0, 'EAST');
      robot.move();
      expect(robot.report()).toBe('4,0,EAST');
    });

    it('should not move WEST if it would fall off the table', () => {
      robot.place(0, 0, 'WEST');
      robot.move();
      expect(robot.report()).toBe('0,0,WEST');
    });

    it('should not move if the robot is at the table boundary', () => {
      robot.place(0, 4, 'WEST');
      robot.move();
      expect(robot.report()).toBe('0,4,WEST');
    });
  });

  describe('left', () => {
    it('should rotate the robot 90 degrees left from NORTH to WEST', () => {
      robot.place(2, 2, 'NORTH');
      robot.left();
      expect(robot.report()).toBe('2,2,WEST');
    });

    it('should rotate the robot 90 degrees left from WEST to SOUTH', () => {
      robot.place(2, 2, 'WEST');
      robot.left();
      expect(robot.report()).toBe('2,2,SOUTH');
    });

    it('should rotate the robot 90 degrees left from SOUTH to EAST', () => {
      robot.place(2, 2, 'SOUTH');
      robot.left();
      expect(robot.report()).toBe('2,2,EAST');
    });

    it('should rotate the robot 90 degrees left from EAST to NORTH', () => {
      robot.place(2, 2, 'EAST');
      robot.left();
      expect(robot.report()).toBe('2,2,NORTH');
    });

    it('should not rotate if the robot is not placed on the table', () => {
      robot.left();
      expect(robot.report()).toBe('Robot not placed on the table');
    });
  });

  describe('right', () => {
    it('should rotate the robot 90 degrees right from NORTH to EAST', () => {
      robot.place(2, 2, 'NORTH');
      robot.right();
      expect(robot.report()).toBe('2,2,EAST');
    });

    it('should rotate the robot 90 degrees right from EAST to SOUTH', () => {
      robot.place(2, 2, 'EAST');
      robot.right();
      expect(robot.report()).toBe('2,2,SOUTH');
    });

    it('should rotate the robot 90 degrees right from SOUTH to WEST', () => {
      robot.place(2, 2, 'SOUTH');
      robot.right();
      expect(robot.report()).toBe('2,2,WEST');
    });

    it('should rotate the robot 90 degrees right from WEST to NORTH', () => {
      robot.place(2, 2, 'WEST');
      robot.right();
      expect(robot.report()).toBe('2,2,NORTH');
    });

    it('should not rotate if the robot is not placed on the table', () => {
      robot.right();
      expect(robot.report()).toBe('Robot not placed on the table');
    });
  });
  // Write similar tests for left, right, rotate, and other methods

  // Additional scenarios and edge cases can be tested as needed
});
