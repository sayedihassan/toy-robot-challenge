/* eslint-disable prettier/prettier */
import { spawn } from 'child_process';
import { Readable, Writable } from 'stream';

describe('Integration Test', () => {
  const runCommands = (
    inputCommands: string[],
    expectedOutput: string[],
    done: jest.DoneCallback,
  ) => {
    // Spawn the main.ts script as a child process
    const childProcess = spawn('node', ['../dist/bundle.js'], {
      stdio: ['pipe', 'pipe', 'inherit'], // Redirect stdin and stdout
      cwd: __dirname, // Set the current directory
    });

    // Create readable and writable streams to interact with the child process
    const inputStream = new Readable({
      read() {
        inputCommands.forEach((command) => {
          this.push(`${command}\n`);
        });
        this.push(null);
      },
    });
    const outputStream = new Writable({
      write(chunk, encoding, callback) {
        const outputLines = chunk.toString().split(/(\r?\n)/g);
        outputLines
          .map((item: string) => item.replace(/Enter a command:/g, '').trim())
          .filter((item: string) => !!item)
          .forEach((item: string) => {
            if (expectedOutput.length > 0) {
              const expectedLine = expectedOutput.shift();
              expect(item).toBe(expectedLine);
            }
          });
        callback();
      },
    });

    // Pipe the input and output streams to the child process
    inputStream.pipe(childProcess.stdin);
    childProcess.stdout.pipe(outputStream);

    // Handle the child process exit event and call done() when it exits
    childProcess.on('exit', (code) => {
      expect(code).toBe(0); // Ensure the child process exits successfully
      done();
    });
  };

  it('should run the application and process commands set 1', (done) => {
    const inputCommands = ['PLACE 0,0,NORTH', 'MOVE', 'REPORT'];
    const expectedOutput = ['0,1,NORTH', 'Exiting...'];

    runCommands(inputCommands, expectedOutput, done);
  });

  it('should run the application and process commands set 2', (done) => {
    const inputCommands = ['PLACE 0,0,NORTH', 'LEFT', 'REPORT'];
    const expectedOutput = ['0,0,WEST', 'Exiting...'];

    runCommands(inputCommands, expectedOutput, done);
  });

  it('should run the application and process commands set 3', (done) => {
    const inputCommands = [
      'PLACE 1,2,EAST',
      'MOVE',
      'MOVE',
      'LEFT',
      'MOVE',
      'REPORT',
    ];
    const expectedOutput = ['3,3,NORTH', 'Exiting...'];

    runCommands(inputCommands, expectedOutput, done);
  });

  it('should run the application and process commands set 4 showing invalid command', (done) => {
    const inputCommands = ['Invalid_command'];
    const expectedOutput = ['Invalid command', 'Exiting...'];

    runCommands(inputCommands, expectedOutput, done);
  });
});
