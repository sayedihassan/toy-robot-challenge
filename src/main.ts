import ToyRobot from './toyRobot';
import * as readline from 'readline';
import { Commands, CommandsZ } from './types';

// Create an instance of the ToyRobot class.
const robot = new ToyRobot();
/**
 * Handle a user command and perform the corresponding action with the robot.
 * @param {string} command - The user-entered command.
 */
function handleCommand(command: string): void {
  if (!command) {
    console.log('Invalid command');
    return;
  }
  try {
    const parts = command.split(' ');
    const action = CommandsZ.parse(parts[0].toUpperCase());

    switch (action) {
      case Commands.PLACE: {
        const args = parts[1].split(',');
        robot.place(parseInt(args[0]), parseInt(args[1]), args[2]);
        break;
      }
      case Commands.MOVE:
        robot.move();
        break;
      case Commands.LEFT:
        robot.left();
        break;
      case Commands.RIGHT:
        robot.right();
        break;
      case Commands.REPORT: {
        // Get and display the robot's current position and direction.
        const result = robot.report();
        console.log(result);
        break;
      }
      default:
        console.log('Invalid command');
    }
  } catch (e) {
    console.log('Invalid command');
  }
}
// Create a readline interface to read user input.
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});
// Set the prompt message for user input.
rl.setPrompt('Enter a command: ');
rl.prompt();
// Listen for user input and call handleCommand to process it.
rl.on('line', (command) => {
  handleCommand(command);
  rl.prompt();
}).on('close', () => {
  console.log('Exiting...');
  process.exit(0);
});
