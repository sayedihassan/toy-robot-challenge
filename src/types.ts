import * as z from 'zod';

export enum Commands {
  PLACE = 'PLACE',
  MOVE = 'MOVE',
  LEFT = 'LEFT',
  RIGHT = 'RIGHT',
  REPORT = 'REPORT',
}
export const CommandsZ = z.nativeEnum(Commands);

export enum DirectionCommand {
  NORTH = 'NORTH',
  SOUTH = 'SOUTH',
  EAST = 'EAST',
  WEST = 'WEST',
}
export const DirectionCommandZ = z.nativeEnum(DirectionCommand);

export enum Degree {
  NORTH = 0,
  SOUTH = 180,
  EAST = 90,
  WEST = 270,
}
